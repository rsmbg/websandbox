# Web Sandbox

This repo is for learning and testing web development.

## Development and Deployment

Use the following command run a local server and launch a live reload web-page in your browser.
```
npx live-server
```

The gitlab pages URL is:
<https://rsmbg.gitlab.io/websandbox>
