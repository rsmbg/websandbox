console.log('SERVICE WORKER: I am at the start of a Service Worker!');

var version = 'v0::';

// Names of the two caches used in this version of the service worker.
const PRECACHE = version + 'precache';
const RUNTIME_CACHE = version + 'runtime';

var cache_config = {

  // A list of local resources we always want to be cached.
  PRECACHE_URLS : [
    './index.html',
    './', // Alias for index.html
    //'./about/index.html',
    //'./offline/',
    './offline.html',
    'DataFileTest.txt',
    'Bass 14.wav',
    'manifest.webmanifest',
    'stylesheet.css',
    './images/icons-144.png',
    'apple-touch-icon.png',
    './images/icons-192.png',
  ],
  //A regular expression that matches "/", "/20XX/anything", "/about/", "/blog/anything" etc.
  cachePathPattern: /^\/(?:(20[0-9]{2}|about|blog|css|images|js)\/(.+)?)?$|\.(?:txt|css|png|wav)$/,
  offlineImage: '<svg role="img" aria-labelledby="offline-title"'
    + ' viewBox="0 0 400 300" xmlns="http://www.w3.org/2000/svg">'
    + '<title id="offline-title">Offline</title>'
    + '<g fill="none" fill-rule="evenodd"><path fill="#D8D8D8" d="M0 0h400v300H0z"/>'
    + '<text fill="#9B9B9B" font-family="Times New Roman,Times,serif" font-size="72" font-weight="bold">'
    + '<tspan x="93" y="172">offline</tspan></text></g></svg>',
  offlinePage: '/offline.html',
};

function addToCache (cacheKey, request, response) {
  if (response.ok) {
    // IMPORTANT: Clone the response. A response is a stream
    // and because we want the browser to consume the response
    // as well as the cache consuming the response, we need
    // to clone it so we have two streams.
    var copy = response.clone();
    caches.open(cacheKey)
      .then( cache => {
        cache.put(request, copy);
    })
    .then(function() {
      console.log('SERVICE WORKER: fetch response stored in cache. ', request.url);
    });
  }
  return response;
}

self.addEventListener('install', function(event) { 
  console.log('SERVICE WORKER: installing.');
  
  function onInstall () {
    //  Caches -  built-in  promise-based API to cache, find and delete resources,
    return caches
      .open(PRECACHE)
      .then(function(cache) {
        return cache.addAll(cache_config.PRECACHE_URLS);
      })
      .then(self.skipWaiting())
      .then(function() {
        console.log('SERVICE WORKER: install completed!');
      })
      .catch(function() {
        console.log('SERVICE WORKER: install failed!');
      })
  }

  event.waitUntil(
    onInstall()
  );
  

});

self.addEventListener('fetch', function(event) {
  console.log(event.request.url);

  //Function that receives an event and checks it against criteria for being handled
  function shouldHandleFetch (event, opts) {
    var request            = event.request;
    var url                = new URL(request.url);
    var criteria           = {
      matchesPathPattern: opts.cachePathPattern.test(url.pathname),
      isGETRequest      : request.method === 'GET',
      isFromMyOrigin    : url.origin === self.location.origin
    };
    
    //If any of the criteria fail, return false
    return !Object.values(criteria).includes(false)
  }
  
  function unableToResolve (request,opts) {
    /* There's a couple of things we can do here.
       - Test the Accept header and then return one of the `offlineFundamentals`
         e.g: `return caches.match('/some/cached/image.png')`
       - You should also consider the origin. It's easier to decide what
         "unavailable" means for requests against your origins than for requests
         against a third party, such as an ad provider
       - Generate a Response programmaticaly, as shown below, and return that
    */
    console.log('SERVICE WORKER: fetch request failed in network.');
    

    var acceptHeader = request.headers.get('Accept');
    var resourceType = 'static';

    if (acceptHeader.indexOf('text/html') !== -1) {
      resourceType = 'content';
    } else if (acceptHeader.indexOf('image') !== -1) {
      resourceType = 'image';
    }


    if (resourceType === 'image') {
      return new Response(opts.offlineImage,
        { headers: { 'Content-Type': 'image/svg+xml' } }
      );
    } else if (resourceType === 'content') {  
      var offline
      (async () => {
        console.log(await caches.keys());
        offline = await caches.match('/offline.html');
        if (offline) {
          return offline;
        }
      })();
      
      /*
      caches.match('/offline.html')
        .then(function(response) {
          // Cache hit - return response
          if (response) {
            return response;
          }
        });
      */
    }

    /* Here we're creating a response programmatically. The first parameter is the
       response body, and the second one defines the options for the response.
    */
    return new Response('<h1>Service Unavailable</h1>', {
      status: 503,
      statusText: 'Service Unavailable',
      headers: new Headers({
        'Content-Type': 'text/html'
      })
    });
  }


  function onFetch(event, opts) {

    event.respondWith(
    
       caches
         // Use a cache-first strategy, but still fetch and cache for "eventually fresh"
        .match(event.request)
        .then(function(cache_response) {

          var network_response = fetch(event.request)
            // We handle the network request with success and failure scenarios.
            .then(
              function(response) {
            
                // Check if we received a valid response
                if(!response || response.status !== 200 || response.type !== 'basic') {
                  return response;
                }

                addToCache(RUNTIME_CACHE, event.request, response)
                
                console.log('SERVICE WORKER: Fetched from network!');
                return response;
              }
              //,unableToResolve(event.request,opts)
            )    
            // We should catch errors on the fetchedFromNetwork handler as well.
            .catch(caches.match('/offline.html'));//unableToResolve(event.request,opts));

          network_response = caches.match('/offline.html')
          // Cache hit - return response
          if (cache_response) {
            console.log('SERVICE WORKER: Fetched from cache!');
            return cache_response;
          }

          return network_response;
        }
      )
    );
  }
  
  if (shouldHandleFetch(event, cache_config)) {
    console.log("SERVICE WORKER: Running onFetch");
    onFetch(event, cache_config);
  }
});

self.addEventListener('activate', (event) => {
  console.log('SERVICE WORKER: activate event in progress.');

  function onActivate (event, opts) {
    //Do cache stuff here
    return caches
      //This method returns a promise which will resolve to an array of available cache keys.
      .keys()
      .then(cache_keys => {
        var caches_to_delete = cache_keys.filter(cache_key => {
          // Filter by keys that don't start with the latest version prefix.
          return !cache_key.startsWith(version)
        });
        var delete_promises = caches_to_delete.map(cache_to_delete => {
          //Return a promise that's fulfilled when each outdated cache is deleted.
          caches.delete(cache_to_delete)
        });
        return Promise.all(delete_promises)
      })
    
  }

  event.waitUntil(
    onActivate(event, cache_config)
      .then( () => self.clients.claim() )
      .catch(function () {
        console.log('SERVICE WORKER: Delete old caches failed.');
      })
      .then(function() {
        console.log('SERVICE WORKER: activate completed!');
      })
      .catch(function() {
        console.log('SERVICE WORKER: activate failed!');
      })
  );
  
});
