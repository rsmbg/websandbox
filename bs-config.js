module.exports = {
/*
  "server": false,
  "proxy": "https://rsmbg.gitlab.io/websandbox",
  "port": 3000,
  "middleware": false
*/
  "server": {
    "baseDir": "./",
    "middleware": {
	1:null
	},
    "serveStaticOptions": {
      "extensions": ['html']
    }
  }
  ,"https": true

/*https: {
    key: "path-to-custom.key",
    cert: "path-to-custom.crt"
  }
*/
};
/*
{
	"server": {
		"baseDir": "./",
		"routes": {
			"/about": "/about.html"
		},
		"directory": true,
		"serveStatic": ["./","about"],
		"serveStaticOptions": {
			"extensions": "html"
		}
	}
}
*/
